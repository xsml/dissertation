"""
@author Hsin-Ming Lin. winter 2016
@brief Retrieve internal correlations between local custom feature values.
"""
import datetime
import melody
import os
import scipy.stats # to calculate correlation
import warnings

def main(): # user settings
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\Dataset\Test"
    targetFilenameExtension = ".krn"
    increment = 0.01 # default = 0.01
    designatedPartNumber = 1 # default = 1; 1st part = 1, 2nd part =2, ano so on
    windowSize = 10 # amount of notes. must >= 5 to retrieve tessitura and mobility.
    hopSize = 1 # amount of notes
    resultsOutputFilename = "Retrieval of Custom Internal Correlations (part="+str(designatedPartNumber)+", w="+str(windowSize)+", h="+str(hopSize)+").csv" # to write results under childPath.
    problemsOutputFilename = "Failure of Custom Internal Correlations (part="+str(designatedPartNumber)+", w="+str(windowSize)+", h="+str(hopSize)+").csv" # to write problems under childPath.
    dig(motherPath, targetFilenameExtension, increment, designatedPartNumber, windowSize, hopSize, resultsOutputFilename, problemsOutputFilename)
    displayTimestamp(initializationTimestamp)

def dig(motherPath, targetFilenameExtension, increment, designatedPartNumber, windowSize, hopSize, resultsOutputFilename, problemsOutputFilename):
    childPaths = [] # to read files and write results    
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        os.chdir(childPaths[i])
        print('entered '+childPaths[i]) # progress display.
        inputFilenames = []
        for filename in os.listdir(childPaths[i]):
            if filename.endswith(targetFilenameExtension):
                inputFilenames.append(filename)
        if len(inputFilenames) > 0: # not an empty list.
            retrieve(childPaths[i], inputFilenames, targetFilenameExtension, increment, designatedPartNumber, windowSize, hopSize, resultsOutputFilename, problemsOutputFilename)

def retrieve(childPath, inputFilenames, targetFilenameExtension, increment, designatedPartNumber, windowSize, hopSize, resultsOutputFilename, problemsOutputFilename):
    writeHeaderRow(resultsOutputFilename)
    problemsOutputFile = open(str(problemsOutputFilename), 'w') # start writing the record file.
    problemsOutputFile.write('path,filename'+'\n') # write the header row.
    currentFileNumber = 1 # for progress display.
    for inputFilename in inputFilenames:
        startPoint = 0 # to excerpt from the start point and before the end point
        endPoint = windowSize # first end point
        tessituras = []
        mobilities =[]
        registerWidths =[]
        largestEnergyRatioIntervals =[]
        susceptibilities = []
        while True: # windowing function
            try:
                warnings.filterwarnings('error') # turn matching warnings into exceptions.
                thisMelody = melody.Melody(inputFilename, increment, designatedPartNumber, startPoint ,endPoint)
            except:
                problemsOutputFile.write(childPath+','+inputFilename+'\n')
                print('failed to parse file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': '+inputFilename+' at '+str(datetime.datetime.now())) # dispay the filename.
                break
            else:
                warnings.resetwarnings() # discards the effect of all previous calls to filterwarnings().
                tessituras.append(thisMelody.getTessitura())
                mobilities.append(thisMelody.getMobility())
                registerWidths.append(thisMelody.getRegisterWidth())
                largestEnergyRatioIntervals.append(thisMelody.getLargestEnergyRatioInterval())
                susceptibilities.append(thisMelody.getSusceptibility())
                startPoint += hopSize # following start point. Disable this line to activate the cumulative windowing.
                endPoint += hopSize # following end point.
                if endPoint > thisMelody.getLength() - 1: # beyond the melody.
                    correlate(childPath, inputFilename, resultsOutputFilename, tessituras, mobilities, registerWidths, largestEnergyRatioIntervals, susceptibilities) # write result output file
                    print('finished file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': '+inputFilename+' at '+str(datetime.datetime.now())) # progress display.
                    break
        currentFileNumber += 1 # for progress display.
    problemsOutputFile.close() # stop writing the rocord file.

def writeHeaderRow(resultsOutputFilename):
    resultsOutputFile = open(str(resultsOutputFilename), 'w') # start writing the results file.
    resultsOutputFile.write('path,filename,') # basic information
    resultsOutputFile.write('corr.: width and LER interval, corr.: width and tessitura, corr.: width and mobility, corr.: width and sus., corr.: LER interval and tessitura, corr.: LER interval and mobility, corr.: LER interval and sus., corr.: tessitura and mobility, corr.: tessitura and sus., corr.: mobility and sus.,') # correlation coefficien
    resultsOutputFile.write('p-value: width and LER interval, p-value: width and tessitura, p-value: width and mobility, p-value: width and sus., p-value: LER interval and tessitura, p-value: LER interval and mobility, p-value: LER interval and sus., p-value: tessitura and mobility, p-value: tessitura and sus., p-value: mobility and sus.'+'\n') # p-value
    resultsOutputFile.close() # stop writing the results file.

def correlate(childPath, inputFilename, resultsOutputFilename, tessituras, mobilities, registerWidths, largestEnergyRatioIntervals, susceptibilities):
    # correlation coefficient and 2-tailed p-value
    correlation1, pValue1 = scipy.stats.pearsonr(registerWidths, largestEnergyRatioIntervals)
    correlation2, pValue2 = scipy.stats.pearsonr(registerWidths, tessituras)
    correlation3, pValue3 = scipy.stats.pearsonr(registerWidths, mobilities)
    correlation4, pValue4 = scipy.stats.pearsonr(registerWidths, susceptibilities)
    correlation5, pValue5 = scipy.stats.pearsonr(largestEnergyRatioIntervals, tessituras)
    correlation6, pValue6 = scipy.stats.pearsonr(largestEnergyRatioIntervals, mobilities)
    correlation7, pValue7 = scipy.stats.pearsonr(largestEnergyRatioIntervals, susceptibilities)
    correlation8, pValue8 = scipy.stats.pearsonr(tessituras, mobilities)
    correlation9, pValue9 = scipy.stats.pearsonr(tessituras, susceptibilities)
    correlation10, pValue10 = scipy.stats.pearsonr(mobilities, susceptibilities)
    resultsOutputFile = open(str(resultsOutputFilename), 'a') # start appending the results file.
    resultsOutputFile.write(childPath+','+inputFilename+',') # name
    resultsOutputFile.write(str(correlation1)+','+str(correlation2)+','+str(correlation3)+','+str(correlation4)+','+str(correlation5)+','+str(correlation6)+','+str(correlation7)+','+str(correlation8)+','+str(correlation9)+','+str(correlation10)+',') # correlation coefficient
    resultsOutputFile.write(str(pValue1)+','+str(pValue2)+','+str(pValue3)+','+str(pValue4)+','+str(pValue5)+','+str(pValue6)+','+str(pValue7)+','+str(pValue8)+','+str(pValue9)+','+str(pValue10)+'\n') # p-value
    resultsOutputFile.close() # stop appending the results file.

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()