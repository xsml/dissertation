"""
@author Hsin-Ming Lin. spring 2016 ~ spring 2017.
@brief Classify composer according to internal correlations between local music21 feature values.
https://github.com/justmarkham/scikit-learn-videos
https://pythonprogramming.net/machine-learning-tutorials/
"""
import csv
import datetime
import matplotlib.pyplot
import os
import pandas
import warnings
from sklearn import svm
from sklearn import metrics
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline

def main(): # user settings
    initializationTimestamp = datetime.datetime.now()
    path = "D:\\Depository\\Curriculum\\UCSD\\Research\\Retrieval\\20170216 Music21 Internal Correlation from String Quartets\\Binary Balanced"
    filename = "Retrieval of Music21 Internal Correlations (part=3, w=10, h=1).csv"
    featureNamesListPath = "D:\\Depository\\Curriculum\\UCSD\\Research\\Document\\Selected Music21 Feature\\Concurrence"
    featureNamesListFilename = "B0992.csv"
    labelName = 'composer'
    isPeeking = False # include all samples to train. warning: Peeking leads to overoptimistic and exaggerated performance results!
    experimentMode = 4 # 1 = tune KNN; 2 = tune PCA; 3 = observe feature selection; 4 = compare feature amount; else = KNN + LR + SVM.
    neighbors = 10 # k-nearest neighbors (KNN) classification.
    nComponents = None # number of componnents for the principal component analysis (PCA). default = None.
    nFeatures = 992 # number of top features to select. The 'all' option bypasses selection, for use in a parameter search.
    pcaOutputFilename = "Principal Component Analysis.csv" # to tune principal component analysis (PCA).
    featureSelectionOutputFilename = "Feature Scores.csv" # to observe feature selection.
    nFeaturesOutputFilename = "Feature Amount Comparison.csv" # to compare feature amount.
    featureNames = readFeatureNames(featureNamesListPath, featureNamesListFilename)
    classify(path, filename, featureNames, labelName, isPeeking, experimentMode, neighbors, nFeatures, featureSelectionOutputFilename, nComponents, pcaOutputFilename, nFeaturesOutputFilename)
    displayTimestamp(initializationTimestamp)

def readFeatureNames(featureNamesListPath, featureNamesListFilename):
    os.chdir(featureNamesListPath)
    with open(featureNamesListFilename) as csvFile:
        table = list(csv.reader(csvFile)) # copy target to a nested list
    featureNames = []
    for row in table:
        featureNames.append(row[0])
    # featureNames = ['Duration of Melodic Arcs', 'Size of Melodic Arcs', 'Most Common Melodic Interval', 'Repeated Notes', 'Average Melodic Interval', 'Melodic Octaves'] # external global features
    # featureNames = ['corr.: Duration of Melodic Arcs and Size of Melodic Arcs', 'corr.: Most Common Melodic Interval and Repeated Notes', 'corr.: Average Melodic Interval and Size of Melodic Arcs', 'corr.: Average Melodic Interval and Melodic Octaves', 'corr.: Average Melodic Interval and Repeated Notes', 'corr.: Average Melodic Interval and Most Common Melodic Interval'] # internal correlations between local features
    return featureNames

def classify(path, filename, featureNames, labelName, isPeeking, experimentMode, neighbors, nFeatures, featureSelectionOutputFilename, nComponents, pcaOutputFilename, nFeaturesOutputFilename, nFolds=10): # default = 10-fold
    os.chdir(path)
    data = pandas.read_csv(filename)
    data.replace(' nan', 0, inplace=True) # replace ' nan' with 0.
    features = data[featureNames]
    labels = data[labelName]
    if isPeeking: # warning: Peeking leads to overoptimistic and exaggerated performance results!
        featuresTrain = features
        featuresTest = features
        labelsTrain = labels
        labelsTest = labels
    else: # no peeking.
        featuresTrain, featuresTest, labelsTrain, labelsTest = train_test_split(features, labels, test_size=0.2) # default = 0.2. hold 20% samples for test.
    if experimentMode == 1: # tune the KNN k-value.
        tuneKNN(featuresTrain, labelsTrain, nFolds) # tune k-nearest neighbors.
    elif experimentMode == 2: # tune the principal component analysis (PCA).
        pca = analyzePrincipalComponent(pcaOutputFilename, featuresTrain, nComponents) # principal component analysis (PCA).
        transformedFeaturesTrain = pca.transform(featuresTrain) # transform the train features.
        classifier = svm.SVC(kernel='linear').fit(transformedFeaturesTrain, labelsTrain) # train the support vector machine (SVM).
        transformedFeaturesTest = pca.transform(featuresTest) # transform the test features.
        print('SVM accuracy after pca = '+str(classifier.score(transformedFeaturesTest, labelsTest))) # display the mean accuracy on the given test data and labels.
    elif experimentMode == 3: # observe the feature selection.
        featureSelection = selectFeatures(featuresTrain, labelsTrain, nFeatures, featureSelectionOutputFilename)
        transformedFeaturesTrain = featureSelection.transform(featuresTrain) # transform the train features.
        classifier = svm.SVC(kernel='linear').fit(transformedFeaturesTrain, labelsTrain) # train the support vector machine (SVM).
        transformedFeaturesTest = featureSelection.transform(featuresTest) # transform the test features.
        print('SVM accuracy after feature selection= '+str(classifier.score(transformedFeaturesTest, labelsTest))) # display the mean accuracy on the given test data and labels.
    elif experimentMode == 4: # compare numbers of features selected.
        nFeaturesOutputFile = open(str(nFeaturesOutputFilename), 'w') # start writing the results file.
        nFeaturesOutputFile.write('amount,accuracy'+'\n') # basic information
        nFeaturesOutputFile.close() # stop writing the results file.
        warnings.filterwarnings('ignore') # never print matching warnings.
        for i in range(0, nFeatures): # featureAmount = new nFeatures.
            validate(features, labels, neighbors, nFolds, i+1, nFeaturesOutputFilename) # n-fold cross validation.
        warnings.resetwarnings() # discards the effect of all previous calls to filterwarnings().        
    else: # default option
        validate(features, labels, neighbors, nFolds, nFeatures) # n-fold cross validation.

def tuneKNN(features, labels, folds): # K parameter tuning.
    kRange = range(1, 51) # modify the range to test other values.
    kScores = []
    for k in kRange:
        knn = KNeighborsClassifier(n_neighbors=k)
        scores = cross_val_score(knn, features, labels, cv=folds)
        kScores.append(scores.mean())
    plot(kRange, kScores)

def plot(kRange, kScores): # plot the value of K for KNN (x-axis) versus the cross-validated accuracy (y-axis)
    matplotlib.pyplot.plot(kRange, kScores)
    matplotlib.pyplot.xlabel('Value of K for KNN')
    matplotlib.pyplot.ylabel('Cross-Validated Accuracy')
    matplotlib.pyplot.show()
    matplotlib.pyplot.close() # delet all references to the figure and release the memory

def analyzePrincipalComponent(pcaOutputFilename, features, nComponents=None): # principal component analysis (PCA).
    pca = PCA(nComponents).fit(features) # principal component analysis.
    pcaOutputFile = open(str(pcaOutputFilename), 'w') # start writing the PCA file.
    for i in range(len(pca.explained_variance_ratio_)):
        pcaOutputFile.write(str(pca.explained_variance_ratio_[i])+'\n')
    pcaOutputFile.close() # stop writing the PCA file.
    return pca

def selectFeatures(features, labels, nFeatures, featureSelectionOutputFilename): # select top features by ANOVA F-value between label/feature for classification tasks.
    featureSelection = SelectKBest(k=nFeatures).fit(features, labels) # k = number of top features to select. The 'all' option bypasses selection, for use in a parameter search.
    featureSelectionOutputFile = open(str(featureSelectionOutputFilename), 'w') # start writing the feature selection file.
    for i in range(len(featureSelection.scores_)):
        featureSelectionOutputFile.write(str(featureSelection.scores_[i])+'\n')
    featureSelectionOutputFile.close() # stop writing the feature selection file.
    return featureSelection

def validate(features, labels, neighbors, folds, nFeatures, nFeaturesOutputFilename=None): # n-fold cross validation
    if nFeaturesOutputFilename is None: # not to compare feature amount.
        # k-nearest neighbors
        classifier = make_pipeline(SelectKBest(k=nFeatures).fit(features, labels), KNeighborsClassifier(neighbors)) # k = number of top features to select. Select top features by ANOVA F-value between label/feature for classification tasks.
        knnAccuracy = cross_val_score(classifier, features, labels, cv=folds).mean() # evaluate a score by cross-validation.
        predicted  = cross_val_predict(classifier, features, labels, cv=folds) # generate cross-validated estimates for each input data point.
        knnPredictionAccuracy = metrics.accuracy_score(labels, predicted)
        # logistic regression    
        classifier = make_pipeline(SelectKBest(k=nFeatures).fit(features, labels), LogisticRegression()) # k = number of top features to select. Select top features by ANOVA F-value between label/feature for classification tasks.
        lrAccuracy = cross_val_score(classifier, features, labels, cv=folds).mean() # evaluate a score by cross-validation.
        predicted  = cross_val_predict(classifier, features, labels, cv=folds) # generate cross-validated estimates for each input data point.
        lrPredictionAccuracy = metrics.accuracy_score(labels, predicted)
        # support vector machine
        classifier = make_pipeline(SelectKBest(k=nFeatures).fit(features, labels), svm.SVC(kernel='linear')) # k = number of top features to select. Select top features by ANOVA F-value between label/feature for classification tasks.
        svmAccuracy = cross_val_score(classifier, features, labels, cv=folds).mean() # evaluate a score by cross-validation.
        predicted  = cross_val_predict(classifier, features, labels, cv=folds) # generate cross-validated estimates for each input data point.
        svmPredictionAccuracy = metrics.accuracy_score(labels, predicted)
        print('KNN '+str(folds)+'-fold cross validation accuracy = '+str(knnAccuracy)+'; K = '+str(neighbors))
        print('LR  '+str(folds)+'-fold cross validation accuracy = '+str(lrAccuracy))
        print('SVM '+str(folds)+'-fold cross validation accuracy = '+str(svmAccuracy))
        print('KNN '+str(folds)+'-fold cross validation prediction accuracy = '+str(knnPredictionAccuracy)+'; K = '+str(neighbors))
        print('LR  '+str(folds)+'-fold cross validation prediction accuracy = '+str(lrPredictionAccuracy))
        print('SVM '+str(folds)+'-fold cross validation prediction accuracy = '+str(svmPredictionAccuracy))
        print('with selected top '+str(nFeatures)+' features')
    else: # need to compare feature amount.
        classifier = make_pipeline(SelectKBest(k=nFeatures).fit(features, labels), svm.SVC(kernel='linear')) # k = number of top features to select. Select top features by ANOVA F-value between label/feature for classification tasks.
        svmAccuracy = cross_val_score(classifier, features, labels, cv=folds).mean() # evaluate a score by cross-validation.
        nFeaturesOutputFile = open(str(nFeaturesOutputFilename), 'a') # start appending the results file.
        nFeaturesOutputFile.write(str(nFeatures)+','+str(svmAccuracy)+'\n') # basic information
        nFeaturesOutputFile.close() # stop writing the results file.
        print(str(nFeatures)+', '+str(svmAccuracy))

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()