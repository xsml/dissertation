"""
@author Hsin-Ming Lin. fall 2015 ~ winter 2016
@brief Draw melodies' susceptibility profiles.
"""
import datetime
import matplotlib.pyplot
import melody
import os
import warnings

def main(): # user settings
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\Dataset\Test"
    targetFilenameExtension = ".krn"
    increment = 0.01 # default = 0.01
    designatedPartNumber = 1 # default = 1; 1st part = 1, 2nd part =2, ano so on
    windowSize = 10 # amount of notes
    hopSize = 1 # amount of notes
    dig(motherPath, targetFilenameExtension, increment, designatedPartNumber, windowSize, hopSize)
    displayTimestamp(initializationTimestamp)

def dig(motherPath, targetFilenameExtension, increment, designatedPartNumber, windowSize, hopSize):
    childPaths = [] # to read files and write results    
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        os.chdir(childPaths[i])
        print('entered '+childPaths[i]) # progress display.
        inputFilenames = []
        for filename in os.listdir(childPaths[i]):
            if filename.endswith(targetFilenameExtension):
                inputFilenames.append(filename)
        if len(inputFilenames) > 0: # not an empty list.
            retrieve(inputFilenames, targetFilenameExtension, increment, designatedPartNumber, windowSize, hopSize)

def retrieve(inputFilenames, targetFilenameExtension, increment, designatedPartNumber, windowSize, hopSize):
    for inputFilename in inputFilenames:
        startPoint = 0 # for excerpt from the start point and before the end point
        endPoint = windowSize - 1 # first end point
        noteNumbers = [] # representative note numbers of each window
        tessituras = [] # data to draw
        mobilities = [] # data to draw
        registerWidths = [] # data to draw
        susceptibilities = [] # data to draw
        while True:
            try:
                warnings.filterwarnings("error")
                thisMelody = melody.Melody(inputFilename, increment, designatedPartNumber, startPoint ,endPoint)
            except:
                print('failed to parse '+str(inputFilename)+' at '+str(datetime.datetime.now()))
                break
            else:
                warnings.filterwarnings("default")
                noteNumbers.append(int((startPoint+endPoint)/2)) # first note is number 0. A list index must be an integer.
                # tessituras.append(thisMelody.getTessitura())
                # mobilities.append(thisMelody.getMobility() * 10) # enlarged by 10 for better visualization
                registerWidths.append(thisMelody.getRegisterWidth())
                susceptibilities.append(thisMelody.getSusceptibility())
                startPoint += hopSize # following start points. Disable this line to activate the cumulative windowing.
                endPoint += hopSize # following end points
                if endPoint > thisMelody.getLength() - 1: # beyond the length of whole melody
                    measureNumbers = [] # representative measure numbers of each window
                    for i in noteNumbers: # Each value of noteNumbers must be within the melody.
                        thisMelody = melody.Melody(inputFilename, increment, designatedPartNumber) # to retrieve all the measure numbers from the whole melody without segmentation
                        measureNumbers.append(thisMelody.getMeasureNumbers()[i])
                    draw(inputFilename, measureNumbers, tessituras, mobilities, registerWidths, susceptibilities, windowSize, hopSize)
                    print('drew and exported '+str(inputFilename)+' at '+str(datetime.datetime.now()))
                    break

def draw(inputFilename, measureNumbers, tessituras, mobilities, registerWidths, susceptibilities, windowSize, hopSize):
    matplotlib.pyplot.figure(figsize=(16,9))
    # matplotlib.pyplot.plot(measureNumbers, tessituras, label='tessitura', color='magenta')
    # matplotlib.pyplot.plot(measureNumbers, mobilities, label='mobility*10', color='cyan')
    matplotlib.pyplot.plot(measureNumbers, registerWidths, label='width', color='green')
    matplotlib.pyplot.plot(measureNumbers, susceptibilities, label='susceptibility', color='blue')
    matplotlib.pyplot.title(inputFilename)
    matplotlib.pyplot.xlabel("measue")
    matplotlib.pyplot.ylabel("value")
    matplotlib.pyplot.legend()
    # matplotlib.pyplot.show()
    matplotlib.pyplot.savefig(inputFilename[:-4]+' w='+str(windowSize)+' h='+str(hopSize)+'.pdf', bbox_inches='tight')
    matplotlib.pyplot.close() # delet all references to the figure and release the memory

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()