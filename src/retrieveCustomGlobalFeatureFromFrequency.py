"""
@author Hsin-Ming Lin. winter 2016
@brief Retrieve custom features from frequency values from files in target folder.
"""
import csv
import datetime
import math # to calculate log
import melodyFrequency
import numpy
import os
import scipy.stats # to calculate correlation

def main(): # user settings.
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\Dataset\Test"
    targetFilenameExtension = ".csv"
    cloumn = 1 # target column number (starts from 0)
    increment = 0.01 # default = 0.01
    resultsOutputFilename = "Retrieval of Custom Global Features.txt" # to write results under childPath.
    problemsOutputFilename = "Failure of Custom Global Features.txt" # to write problems under childPath.
    summaryOutputFilename = "Summary of Custom Global Features.txt" # to write summary under motherPath.
    writeSummaryHeaderRow(motherPath, summaryOutputFilename)
    dig(motherPath, targetFilenameExtension, cloumn, increment, resultsOutputFilename, problemsOutputFilename, summaryOutputFilename)
    displayTimestamp(initializationTimestamp)

def writeSummaryHeaderRow(motherPath, summaryOutputFilename): # write the header only once; append summary many times by other functions.
    os.chdir(motherPath)
    summaryOutputFile = open(str(summaryOutputFilename), 'w') # start writing the summary file.
    summaryOutputFile.write('path,sample amount,retrieved amount,mean length,mean highest frequency,mean lowest frequency,mean central frequency,') # name
    summaryOutputFile.write('mean register width,mean LER interval,mean susceptibility,') # mean
    summaryOutputFile.write('median register width,median LER interval,median susceptibility,') # median
    summaryOutputFile.write('maximal register width,maximal LER interval,maximal susceptibility,') # maximum
    summaryOutputFile.write('minimal register width,minimal LER interval,minimal susceptibility,') # minimum
    summaryOutputFile.write('register width SD,LER interval SD,susceptibility SD,') # standard deviation
    summaryOutputFile.write('register width CV,LER interval CV,susceptibility CV,') # coefficient of variation
    summaryOutputFile.write('corr.: length and width, corr.: length and LER interval, corr.: length and sus., corr.: width and LER interval, corr.: width and sus., corr.: LER interval and sus.,') # correlation coefficient
    summaryOutputFile.write('p-value: length and width, p-value: length and LER interval, p-value: length and sus., p-value: width and LER interval, p-value: width and sus., p-value: LER interval and sus.'+'\n') # p-value
    summaryOutputFile.close() # stop writing the summary file.

def dig(motherPath, targetFilenameExtension, cloumn, increment, resultsOutputFilename, problemsOutputFilename, summaryOutputFilename):
    childPaths = [] # to read files and write results    
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        os.chdir(childPaths[i])
        print('entered '+childPaths[i]) # progress display.
        inputFilenames = []
        for filename in os.listdir(childPaths[i]):
            if filename.endswith(targetFilenameExtension):
                inputFilenames.append(filename)
        if len(inputFilenames) > 0: # not an empty list.
            retrieve(motherPath, childPaths[i], inputFilenames, targetFilenameExtension, cloumn, increment, resultsOutputFilename, problemsOutputFilename, summaryOutputFilename)

def retrieve(motherPath, childPath, inputFilenames, targetFilenameExtension, cloumn, increment, resultsOutputFilename, problemsOutputFilename, summaryOutputFilename):
    lengths = []
    logHighestFrequencies = []
    logLowestFrequencies = []
    registerWidths = []
    logCentralFrequencies = []
    largestEnergyRatioIntervals = []
    susceptibilities = []
    # step 1 of 2: wirte results
    resultsOutputFile = open(str(resultsOutputFilename), 'w') # start writing the results file.
    resultsOutputFile.write('path,filename,length,highest frequency,lowest frequency,central frequency,register width,largest energy ratio interval,susceptibility'+'\n') # write the header row.
    problemsOutputFile = open(str(problemsOutputFilename), 'w') # start writing the record file.
    problemsOutputFile.write('path,filename'+'\n') # write the header row.
    retrievedFileNumber = 0 # for retrieval summary.
    currentFileNumber = 1  # for progress display.
    for inputFilename in inputFilenames:
        frequencyList = []
        with open(inputFilename) as inputFile:
            table = list(csv.reader(inputFile)) # copy the input file to a nested list
        for j in range(len(table)):
            if float(table[j][cloumn]) != 0: # (row, cloumn) coordinate
                frequencyList.append(float(table[j][cloumn])) # (row, cloumn) coordinate
        try:
            thisMelody = melodyFrequency.Melody(frequencyList, increment)
        except:
            problemsOutputFile.write(childPath+','+inputFilename+'\n')
            print('file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': failed to parse '+inputFilename) # dispay the filename.
        else:
            lengths.append(thisMelody.getLength())
            logHighestFrequencies.append(math.log(thisMelody.getHighestFrequency(), 2)) # log2 for later mean and median calculation
            logLowestFrequencies.append(math.log(thisMelody.getLowestFrequency(), 2)) # log2 for later mean and median calculation
            logCentralFrequencies.append(math.log(thisMelody.getCentralFrequency(), 2)) # log2 for later mean and median calculation
            registerWidths.append(thisMelody.getRegisterWidth())
            largestEnergyRatioIntervals.append(thisMelody.getLargestEnergyRatioInterval())
            susceptibilities.append(thisMelody.getSusceptibility())
            resultsOutputFile.write(childPath+','+inputFilename+','+str(thisMelody.getLength())+','+str(thisMelody.getHighestFrequency())+','+str(thisMelody.getLowestFrequency())+','+str(thisMelody.getCentralFrequency())+',')
            resultsOutputFile.write(str(thisMelody.getRegisterWidth())+','+str(thisMelody.getLargestEnergyRatioInterval())+','+str(thisMelody.getSusceptibility())+'\n')
            print('file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': susceptibility = '+str(thisMelody.getSusceptibility())+'; '+inputFilename) # progress display.
            retrievedFileNumber += 1 # for retrieval summary.
        finally:
            currentFileNumber += 1 # for progress display.
    resultsOutputFile.close() # stop writing the results file.
    problemsOutputFile.close() # stop writing the rocord file.
    # step 2 of 2: append correlation coefficients and 2-tailed p-values
    correlation1, pValue1 = (scipy.stats.pearsonr(lengths, registerWidths))
    correlation2, pValue2 = (scipy.stats.pearsonr(lengths, largestEnergyRatioIntervals))
    correlation3, pValue3 = (scipy.stats.pearsonr(lengths, susceptibilities))
    correlation4, pValue4 = (scipy.stats.pearsonr(registerWidths, largestEnergyRatioIntervals))
    correlation5, pValue5 = (scipy.stats.pearsonr(registerWidths, susceptibilities))
    correlation6, pValue6 = (scipy.stats.pearsonr(largestEnergyRatioIntervals, susceptibilities))
    os.chdir(motherPath)
    summaryOutputFile = open(str(summaryOutputFilename), 'a') # start appending the summary file.
    summaryOutputFile.write(childPath+','+str(len(inputFilenames))+','+str(retrievedFileNumber)+','+str(numpy.mean(lengths))+','+str(2**numpy.mean(logHighestFrequencies))+','+str(2**numpy.mean(logLowestFrequencies))+','+str(2**numpy.mean(logCentralFrequencies))+',') # name
    summaryOutputFile.write(str(numpy.mean(registerWidths))+','+str(numpy.mean(largestEnergyRatioIntervals))+','+str(numpy.mean(susceptibilities))+',') # mean
    summaryOutputFile.write(str(numpy.median(registerWidths))+','+str(numpy.median(largestEnergyRatioIntervals))+','+str(numpy.median(susceptibilities))+',') # median
    summaryOutputFile.write(str(max(registerWidths))+','+str(max(largestEnergyRatioIntervals))+','+str(max(susceptibilities))+',') # maximum
    summaryOutputFile.write(str(min(registerWidths))+','+str(min(largestEnergyRatioIntervals))+','+str(min(susceptibilities))+',') # minimum
    summaryOutputFile.write(str(numpy.std(registerWidths))+','+str(numpy.std(largestEnergyRatioIntervals))+','+str(numpy.std(susceptibilities))+',') # standard deviation
    summaryOutputFile.write(str(numpy.std(registerWidths)/numpy.mean(registerWidths))+','+str(numpy.std(largestEnergyRatioIntervals)/numpy.mean(largestEnergyRatioIntervals))+','+str(numpy.std(susceptibilities)/numpy.mean(susceptibilities))+',') # coefficient of variation
    summaryOutputFile.write(str(correlation1)+','+str(correlation2)+','+str(correlation3)+','+str(correlation4)+','+str(correlation5)+','+str(correlation6)+',') # correlation coefficient
    summaryOutputFile.write(str(pValue1)+','+str(pValue2)+','+str(pValue3)+','+str(pValue4)+','+str(pValue5)+','+str(pValue6)+'\n') # pValue
    summaryOutputFile.close() # stop appending the summary file.

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()