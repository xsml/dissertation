def cross_val(data, target, C):
    from sklearn.model_selection import cross_val_score
    scores = []
    for x in C:
        clf = svm.SVC(kernel='linear', C=x)
        s = np.array(cross_val_score(clf, data, target, cv=5))
        scores.append(np.mean(s))
        
    mx_score = max(scores)
    idx = [i for i, j in enumerate(scores) if j == mx_score]
    print(scores)
    print(C[idx[0]])
    return C[idx[0]]

# load required packages
import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline
from keras.utils import np_utils
#from sklearn import svm, metrics
from sklearn.svm import SVC
# load required functionality from keras
from keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()
# reshape the data: (n_samples, 28, 28) => (n_samples, 28*28)
X_train = X_train.reshape(-1, 28 * 28)
X_test = X_test.reshape(-1, 28 * 28)

# convert data type and normalize the values (8-bit = 256 = 0...255)
X_train = X_train.astype('float32') / 255.0 *2 - 1
X_test = X_test.astype('float32') / 255.0 *2 - 1


# convert the class labels to 10-dimensional class arrays:
# - before: y_train = (n_samples, )
# - after: Y_train = (n_samples, 10)
#
#Y_train = np_utils.to_categorical(y_train, 10)
#Y_test = np_utils.to_categorical(y_test, 10)


# take all of it - make that number lower for experiments
#examples = 200

print("Cross Validation....")
#best_C = cross_val(X_train[:examples][:], y_train[:examples], [0.1, 1, 10])
best_C = cross_val(X_train, y_train, [0.1, 1, 10])

clf = SVC(probability=False,  # cache_size=200,
              kernel="linear", C=best_C)

print("Start fitting....")
#clf.fit(X_train[:examples][:], y_train[:examples])
clf.fit(X_train, y_train)

from sklearn import metrics
predicted = clf.predict(X_test)
print("Confusion matrix:\n%s" %
          metrics.confusion_matrix(y_test,
                                   predicted))
clf.score(X_test, y_test)