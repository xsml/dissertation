"""
@author Hsin-Ming Lin. spring 2014 ~ winter 2016
@brief Retrieve custom features from files in target folders.
"""
import datetime
import math # to calculate log
import melody
import numpy # to calculate basic statistical information
import os
import scipy.stats # to calculate correlation coefficient

def main(): # user settings.
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\\Depository\\Curriculum\\UCSD\\Major\\Dissertation\\Composition"
    targetFilenameExtension = ".xml"
    increment = 0.01 # default = 0.01
    designatedPartNumber = 1 # default = 1; 1= 1st part, 2= 2nd part, ano so on.
    resultsOutputFilename = "Retrieval of Custom Global Features (part="+str(designatedPartNumber)+").csv" # to write results under childPath.
    problemsOutputFilename = "Failure of Custom Global Features (part="+str(designatedPartNumber)+").csv" # to write problems under childPath.
    summaryOutputFilename = "Summary of Custom Global Features (part="+str(designatedPartNumber)+").csv" # to write summary under motherPath.
    writeSummaryHeaderRow(motherPath, summaryOutputFilename)
    dig(motherPath, targetFilenameExtension, increment, designatedPartNumber, resultsOutputFilename, problemsOutputFilename, summaryOutputFilename)
    displayTimestamp(initializationTimestamp)

def writeSummaryHeaderRow(motherPath, summaryOutputFilename): # write header row only once; append summary many times by other functions.
    os.chdir(motherPath)
    summaryOutputFile = open(str(summaryOutputFilename), 'w') # start writing the summary file.
    summaryOutputFile.write('path,sample amount,retrieved amount,mean length,mean highest frequency,mean lowest frequency,mean central frequency,') # name
    summaryOutputFile.write('mean register width,mean LER interval,mean tessitura,mean mobility,mean susceptibility,') # mean
    summaryOutputFile.write('median register width,median LER interval,median tessitura,median mobility,median susceptibility,') # median
    summaryOutputFile.write('maximal register width,maximal LER interval,maximal tessitura,maximal mobility,maximal susceptibility,') # maximum
    summaryOutputFile.write('minimal register width,minimal LER interval,minimal tessitura,minimal mobility,minimal susceptibility,') # minimum
    summaryOutputFile.write('register width SD,LER interval SD,tessitura SD,mobility SD,susceptibility SD,') # standard deviation
    summaryOutputFile.write('register width CV,LER interval CV,tessitura CV,mobility CV,susceptibility CV,') # coefficient of variation
    summaryOutputFile.write('corr.: length and width, corr.: length and LER interval, corr.: length and tessituras, corr.: length and mobility, corr.: length and sus., corr.: width and LER interval, corr.: width and tessitura, corr.: width and mobility, corr.: width and sus., corr.: LER interval and tessitura, corr.: LER interval and mobility, corr.: LER interval and sus., corr.: tessitura and mobility, corr.: tessitura and sus., corr.: mobility and sus.,') # correlation coefficient
    summaryOutputFile.write('p-value: length and width, p-value: length and LER interval, p-value: length and tessituras, p-value: length and mobility, p-value: length and sus., p-value: width and LER interval, p-value: width and tessitura, p-value: width and mobility, p-value: width and sus., p-value: LER interval and tessitura, p-value: LER interval and mobility, p-value: LER interval and sus., p-value: tessitura and mobility, p-value: tessitura and sus., p-value: mobility and sus.'+'\n') # p-value
    summaryOutputFile.close() # stop writing the summary file.

def dig(motherPath, targetFilenameExtension, increment, designatedPartNumber, resultsOutputFilename, problemsOutputFilename, summaryOutputFilename):
    childPaths = [] # to read files and write results    
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        os.chdir(childPaths[i])
        print('entered '+childPaths[i]) # progress display.
        inputFilenames = []
        for filename in os.listdir(childPaths[i]):
            if filename.endswith(targetFilenameExtension):
                inputFilenames.append(filename)
        if len(inputFilenames) > 0: # not an empty list.
            retrieve(childPaths[i], inputFilenames, increment, designatedPartNumber, resultsOutputFilename, problemsOutputFilename, motherPath, summaryOutputFilename)
    
def retrieve(childPath, inputFilenames, increment, designatedPartNumber, resultsOutputFilename, problemsOutputFilename, motherPath, summaryOutputFilename): # core function
    lengths = []
    tessituras = []
    mobilities = []
    logHighestFrequencies = []
    logLowestFrequencies = []
    registerWidths = []
    logCentralFrequencies = []
    largestEnergyRatioIntervals = []
    susceptibilities = []
    # step 1 of 2: wirte results
    resultsOutputFile = open(str(resultsOutputFilename), 'w') # start writing the results file.
    resultsOutputFile.write('path,filename,length,highest frequency,lowest frequency,central frequency,register width,largest energy ratio interval,tessitura,mobility,susceptibility'+'\n') # write the header row.
    problemsOutputFile = open(str(problemsOutputFilename), 'w') # start writing the record file.
    problemsOutputFile.write('path,filename'+'\n') # write the header row.
    retrievedFileNumber = 0 # for retrieval summary.
    currentFileNumber = 1  # for progress display.
    for inputFilename in inputFilenames:
        try:
            thisMelody = melody.Melody(inputFilename, increment, designatedPartNumber)
        except:
            problemsOutputFile.write(childPath+','+inputFilename+'\n')
            print('file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': failed to parse '+inputFilename) # dispay the filename.
        else:
            lengths.append(thisMelody.getLength())
            tessituras.append(thisMelody.getTessitura())
            mobilities.append(thisMelody.getMobility())
            logHighestFrequencies.append(math.log(thisMelody.getHighestFrequency(), 2)) # log2 for later mean and median calculation
            logLowestFrequencies.append(math.log(thisMelody.getLowestFrequency(), 2)) # log2 for later mean and median calculation
            logCentralFrequencies.append(math.log(thisMelody.getCentralFrequency(), 2)) # log2 for later mean and median calculation
            registerWidths.append(thisMelody.getRegisterWidth())
            largestEnergyRatioIntervals.append(thisMelody.getLargestEnergyRatioInterval())
            susceptibilities.append(thisMelody.getSusceptibility())
            resultsOutputFile.write(childPath+','+inputFilename+','+str(thisMelody.getLength())+','+str(thisMelody.getHighestFrequency())+','+str(thisMelody.getLowestFrequency())+','+str(thisMelody.getCentralFrequency())+',')
            resultsOutputFile.write(str(thisMelody.getRegisterWidth())+','+str(thisMelody.getLargestEnergyRatioInterval())+','+str(thisMelody.getTessitura())+','+str(thisMelody.getMobility())+','+str(thisMelody.getSusceptibility())+'\n')
            print('file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': susceptibility = '+str(thisMelody.getSusceptibility())+'; '+inputFilename) # progress display.
            retrievedFileNumber += 1 # for retrieval summary.
        finally:
            currentFileNumber += 1 # for progress display.
    resultsOutputFile.close() # stop writing the results file.
    problemsOutputFile.close() # stop writing the rocord file.
    # step 2 of 2: append correlation coefficients and 2-tailed p-values
    correlation1, pValue1 = scipy.stats.pearsonr(lengths, registerWidths)
    correlation2, pValue2 = scipy.stats.pearsonr(lengths, largestEnergyRatioIntervals)
    correlation3, pValue3 = scipy.stats.pearsonr(lengths, tessituras)
    correlation4, pValue4 = scipy.stats.pearsonr(lengths, mobilities)
    correlation5, pValue5 = scipy.stats.pearsonr(lengths, susceptibilities)
    correlation6, pValue6 = scipy.stats.pearsonr(registerWidths, largestEnergyRatioIntervals)
    correlation7, pValue7 = scipy.stats.pearsonr(registerWidths, tessituras)
    correlation8, pValue8 = scipy.stats.pearsonr(registerWidths, mobilities)
    correlation9, pValue9 = scipy.stats.pearsonr(registerWidths, susceptibilities)
    correlation10, pValue10 = scipy.stats.pearsonr(largestEnergyRatioIntervals, tessituras)
    correlation11, pValue11 = scipy.stats.pearsonr(largestEnergyRatioIntervals, mobilities)
    correlation12, pValue12 = scipy.stats.pearsonr(largestEnergyRatioIntervals, susceptibilities)
    correlation13, pValue13 = scipy.stats.pearsonr(tessituras, mobilities)
    correlation14, pValue14 = scipy.stats.pearsonr(tessituras, susceptibilities)
    correlation15, pValue15 = scipy.stats.pearsonr(mobilities, susceptibilities)
    os.chdir(motherPath)
    summaryOutputFile = open(str(summaryOutputFilename), 'a') # start appending the summary file.
    summaryOutputFile.write(childPath+','+str(len(inputFilenames))+','+str(retrievedFileNumber)+','+str(numpy.mean(lengths))+','+str(2**numpy.mean(logHighestFrequencies))+','+str(2**numpy.mean(logLowestFrequencies))+','+str(2**numpy.mean(logCentralFrequencies))+',') # name
    summaryOutputFile.write(str(numpy.mean(registerWidths))+','+str(numpy.mean(largestEnergyRatioIntervals))+','+str(numpy.mean(tessituras))+','+str(numpy.mean(mobilities))+','+str(numpy.mean(susceptibilities))+',') # mean
    summaryOutputFile.write(str(numpy.median(registerWidths))+','+str(numpy.median(largestEnergyRatioIntervals))+','+str(numpy.median(tessituras))+','+str(numpy.median(mobilities))+','+str(numpy.median(susceptibilities))+',') # median
    summaryOutputFile.write(str(max(registerWidths))+','+str(max(largestEnergyRatioIntervals))+','+str(max(tessituras))+','+str(max(mobilities))+','+str(max(susceptibilities))+',') # maximum
    summaryOutputFile.write(str(min(registerWidths))+','+str(min(largestEnergyRatioIntervals))+','+str(min(tessituras))+','+str(min(mobilities))+','+str(min(susceptibilities))+',') # minimum
    summaryOutputFile.write(str(numpy.std(registerWidths))+','+str(numpy.std(largestEnergyRatioIntervals))+','+str(numpy.std(tessituras))+','+str(numpy.std(mobilities))+','+str(numpy.std(susceptibilities))+',') # standard deviation
    summaryOutputFile.write(str(numpy.std(registerWidths)/numpy.mean(registerWidths))+','+str(numpy.std(largestEnergyRatioIntervals)/numpy.mean(largestEnergyRatioIntervals))+','+str(numpy.std(tessituras)/numpy.mean(tessituras))+','+str(numpy.std(mobilities)/numpy.mean(mobilities))+','+str(numpy.std(susceptibilities)/numpy.mean(susceptibilities))+',') # coefficient of variation
    summaryOutputFile.write(str(correlation1)+','+str(correlation2)+','+str(correlation3)+','+str(correlation4)+','+str(correlation5)+','+str(correlation6)+','+str(correlation7)+','+str(correlation8)+','+str(correlation9)+','+str(correlation10)+','+str(correlation11)+','+str(correlation12)+','+str(correlation13)+','+str(correlation14)+','+str(correlation15)+',') # correlation coefficient
    summaryOutputFile.write(str(pValue1)+','+str(pValue2)+','+str(pValue3)+','+str(pValue4)+','+str(pValue5)+','+str(pValue6)+','+str(pValue7)+','+str(pValue8)+','+str(pValue9)+','+str(pValue10)+','+str(pValue11)+','+str(pValue12)+','+str(pValue13)+','+str(pValue14)+','+str(pValue15)+'\n') # p-value
    summaryOutputFile.close() # stop appending the summary file.

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()