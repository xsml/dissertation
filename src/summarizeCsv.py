"""
@author Hsin-Ming Lin. fall 2015 ~ fall 2016.
@brief statistically summarize (e.g. mean) CSV files.
"""
import csv
import datetime
import numpy
import os

def main(): # user settings.
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\\Dataset\\WJazzD"
    targetFilename = "Retrieval of Music21 Global Features (part=0).csv"
    summaryOutputFilename = "Summary of Music21 Global Features.csv"
    dig(motherPath, targetFilename, summaryOutputFilename)
    displayTimestamp(initializationTimestamp)
    
def dig(motherPath, targetFilename, summaryOutputFilename):
    os.chdir(motherPath)
    summaryOutputFile = open(str(summaryOutputFilename), 'w') # start writing the summary file
    summaryOutputFile.close() # stop writing the summary file but will append later.
    childPaths = [] # to read files and write results    
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        summarize(motherPath, childPaths[i], targetFilename, summaryOutputFilename)

def summarize(motherPath, childPath, targetFilename, summaryOutputFilename):
    os.chdir(childPath) # switch to target path
    print('entered '+childPath) # progress display.
    with open(targetFilename) as csvFile: # open(targetFilename, newline='')
        table = list(csv.reader(csvFile)) # copy target to a nested list
    # step 1 of 3: values
    while '' in table[0]:
        table[0].remove('') # remove empty string from the header
    del table[0][-1] # delete the last column in the fist row = 'test'
    # del table[i][607] # problematic 607th column of all preset features = (QL4) Range of Note Quarter Lengths
    # del table[i][605] # problematic 605th column of all preset features = (QL2) Most Common Note Quarter Length
    for i in range(len(table)):
        if i >= 1: # omit the first row
            for j in range(len(table[0])-1):
                if j >= 1: # omit the first column
                    try:
                        table[i][j] = float(table[i][j]) # (row, cloumn) coordinate
                    except: # TODO: How to handle incorrect data format?
                        print('coordinate of problematic value: ('+str(i)+', '+str(j)+')') # display the coordinate of problematic value
                        print('content of problematic value: ' + str(table[i][j])) # display the content of problematic value
    # step 2 of 3: summaries
    rowAmount = len(table)
    columnAmount = len(table[0])
    summaries = []
    for m in range(columnAmount):
        if m >= 1: # omit the first column
            tempList = [] # temporary list for statistical operation
            for n in range(rowAmount):
                if n >= 1: # omit the first row
                    tempList.append(float(table[n][m])) # (row, cloumn) coordinate, so it is (n, m) instead of (m, n)
            summaries.append(numpy.mean(tempList)) # modify here to change the statistical method
    summaries.insert(0, childPath) # insert dataset path into the list
    summaries.insert(1, rowAmount-1) # insert retrieved sample amount information into the list
    # step 3 of 3: output
    os.chdir(motherPath) # switch to summary output path
    summaryOutputFile = open(str(summaryOutputFilename), 'a') # start appending the summary file
    summaryOutputFile.write(str(summaries)+'\n') # TODO: write the header for once
    summaryOutputFile.close() # stop appending the summary file.

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()