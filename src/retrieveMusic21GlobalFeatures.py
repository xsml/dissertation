"""
@author Hsin-Ming Lin. summer 2015 ~ fall 2016, and fall 2019
@brief Retrieve music21 features from files in target folders.
"""
# import csv
import datetime
import music21
import os

def main(): # user settings.
    initializationTimestamp = datetime.datetime.now()
    motherPath = 'D:\Dataset\Test'
    targetFilenameExtension = '.krn'
    designatedPartNumber = 0 # default = 0; 0 = all parts, 1 = 1st part, 2 = 2nd part, ano so on. +1 for MusicXML.
    featureIdentities = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'P10', 'P11', 'P12', 'P13', 'P14', 'P15', 'P16', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M9', 'M10', 'M11', 'M12', 'M13', 'M14', 'M15', 'M17', 'M18', 'M19'] # create a list of selected feature IDs
    resultsOutputFilename = 'Retrieval of Music21 Global Features (part='+str(designatedPartNumber)+').csv' # to write results into childPath.
    problemsOutputFilename = 'Failure of Music21 Global Features (part='+str(designatedPartNumber)+').csv' # to write problems under childPath.
    dig(motherPath, targetFilenameExtension, designatedPartNumber, featureIdentities, resultsOutputFilename, problemsOutputFilename)
    displayTimestamp(initializationTimestamp)

def dig(motherPath, targetFilenameExtension, designatedPartNumber, featureIdentities, resultsOutputFilename, problemsOutputFilename):
    childPaths = [] # to read files and write results
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        os.chdir(childPaths[i])
        print('entered '+childPaths[i]) # progress display.
        inputFilenames = []
        for filename in os.listdir(childPaths[i]):
            if filename.endswith(targetFilenameExtension):
                inputFilenames.append(filename)
        if len(inputFilenames) > 0: # not an empty list.
            retrieve(childPaths[i], inputFilenames, targetFilenameExtension, designatedPartNumber, featureIdentities, resultsOutputFilename, problemsOutputFilename)

def retrieve(childPath, inputFilenames, targetFilenameExtension, designatedPartNumber, featureIdentities, resultsOutputFilename, problemsOutputFilename):
    resultsOutputFile = open(str(resultsOutputFilename), 'w') # start writing the results file.
    problemsOutputFile = open(str(problemsOutputFilename), 'w') # start writing the record file.
    problemsOutputFile.write('path,filename'+'\n') # write the header row.
    currentFileNumber = 0  # for progress display.
    retrievedFileNumber = 0 # for summary display.
    retrievedFilenames = ['filename'] # for final labelling below
    featureSet = music21.features.extractorsById(featureIdentities) # decode the above list of selected feature IDs
    for inputFilename in inputFilenames:
        dataSet = music21.features.DataSet(classLabel='test') # create a blank dataset
        dataSet.addFeatureExtractors(featureSet) # add feature extractors to the dataset
        currentFileNumber += 1 # for progress display.
        try:
            if designatedPartNumber >= 1: # extract a single part
                parsingResult = music21.converter.parse(inputFilename) # parse the sample file
                score = music21.stream.Score() # create a blank music21 score
                score.append(parsingResult[designatedPartNumber - 1]) # append the designated part to the score
                dataSet.addData(score) # add the score to the dataset
            else: # extract all parts
                dataSet.addData(inputFilename) # add the all parts
            dataSet.process() # process the single sample file (either one part or all parts)
            tempList = dataSet.getString('csv') # TODO: insert a column to add the childPath info
            resultsOutputFile.write(tempList) # intentionally omit new line here for easier text processing later.
            print('file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+' processed') # progress display.
            retrievedFileNumber += 1
            retrievedFilenames.append(inputFilename)
        except:
            problemsOutputFile.write(childPath+','+inputFilename+'\n') # record the problematic filenames
            print('failed to parse or add file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': '+inputFilename+' at '+str(datetime.datetime.now())) # dispay the filename.
    resultsOutputFile.close() # stop appending the results file.
    problemsOutputFile.close() # stop writing the rocord file.
    # addPathInfo(childPath, resultsOutputFilename, retrievedFilenames) # warning: All vaules will become strings.
    print('processed '+str(retrievedFileNumber)+' from '+str(len(inputFilenames))+' files at '+str(datetime.datetime.now())) # summary display.

#def addPathInfo(childPath, resultsOutputFilename, retrievedFilenames): # warning: All vaules will become strings.
#    with open(resultsOutputFilename) as target: # open(targetFilename, newline='')
#        table = list(csv.reader(target)) # copy target to a nested list
#    table[0].insert(0, 'path') # insert a column into the heaer row.
#    for i in range(len(table)):
#        if i == 0: # the header row
#            del table[i][-1] # delete the last column = 'test'
#        else: # other rows
#            del table[i][len(table[0])-1:] # delete the redundant columns resulting from undesirable repeated header rows.
#            table[i].insert(0, childPath) # insert dataset path into the list
#    resultsOutputFile = open(str(resultsOutputFilename), 'w') # start writing the results file.
#    for i in range(len(table)):
#        resultsOutputFile.write(str(table[i])+'\n')
#    resultsOutputFile.close() # stop appending the results file.

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()