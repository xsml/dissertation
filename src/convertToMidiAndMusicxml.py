"""
@author: Hsin-Ming Lin. Summer 2014.
@brief Convert designated files to MIDI files and MusicXML files.
"""
import datetime
import music21
import os

def main():
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\Depository"
    targetFilenameExtension = ".krn" # available options: zip, krn, abc, xml, mxl.
    childPaths = []
    for paths, directoryNames, filenames in os.walk(motherPath):
        childPaths.append(paths)
    for childPath in childPaths:
        os.chdir(childPath)
        print('current directory: '+ childPath)
        inputFilenames = []
        for filename in os.listdir(childPath):
            if filename.endswith(targetFilenameExtension):
                inputFilenames.append(filename)
        for inputFilename in inputFilenames:
            print('start to parse ' + inputFilename + ' at ' + str(datetime.datetime.now()))
            score = music21.converter.parse(inputFilename)
            if inputFilename.endswith('.mid'):
                pass # Nothing to do here if the source file is already in MIDI format.
            else: # convert to MIDI file
                print('start to convert to MIDI format')
                outputFilename = inputFilename[:-3] + 'mid'
                score.write('midi', outputFilename)
            if inputFilename.endswith('.xml'):
                pass # Nothting to do here if the source file is already in MusicXML format.
            else: # convert to MusicXML file
                print('start to convert to MusicXML format')
                outputFilename = inputFilename[:-3] + 'xml'
                score.write('musicxml', outputFilename)
    displayTimestamp(initializationTimestamp)

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()