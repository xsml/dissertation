"""
@author Hsin-Ming Lin. spring 2016.
@brief Retrieve internal correlations between local music21 feature values.
"""
import csv
import datetime
import music21
import os
import scipy.stats # to calculate correlation

def main(): # user settings
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\Temporary\Music21 String Quartet Haydn"
    targetFilenameExtension = ".krn"
    designatedPartNumber = 1 # 0 = all parts, 1 = 1st part, 2 = 2nd part, ano so on.
    windowSize = 15 # amount of measure(s)
    hopSize = 1 # amount of measure(s)
    featureIdentities = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'P10', 'P11', 'P12', 'P13', 'P14', 'P15', 'P16', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M9', 'M10', 'M11', 'M12', 'M13', 'M14', 'M15', 'M17', 'M18', 'M19'] # list of music21 feature IDs
    individualOutputFilenameSuffix = "Music21 Internal Correlations (part="+str(designatedPartNumber)+", w="+str(windowSize)+", h="+str(hopSize)+").csv" # to write results under childPath.
    resultsOutputFilename = "Retrieval of "+individualOutputFilenameSuffix # to write results under childPath.
    problemsOutputFilename = "Failure of Music21 Internal Correlations (part="+str(designatedPartNumber)+").csv" # to write problems under childPath.
    dig(motherPath, targetFilenameExtension, designatedPartNumber, windowSize, hopSize, featureIdentities, individualOutputFilenameSuffix, resultsOutputFilename, problemsOutputFilename)
    displayTimestamp(initializationTimestamp)

def dig(motherPath, targetFilenameExtension, designatedPartNumber, windowSize, hopSize, featureIdentities, individualOutputFilenameSuffix, resultsOutputFilename, problemsOutputFilename):
    childPaths = [] # to read files and write results
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        os.chdir(childPaths[i])
        print('entered '+childPaths[i]) # progress display.
        inputFilenames = []
        for filename in os.listdir(childPaths[i]):
            if filename.endswith(targetFilenameExtension):
                inputFilenames.append(filename)
        if len(inputFilenames) > 0: # not an empty list.
            excerpt(childPaths[i], inputFilenames, targetFilenameExtension, designatedPartNumber, windowSize, hopSize, featureIdentities, individualOutputFilenameSuffix, resultsOutputFilename, problemsOutputFilename)

def excerpt(childPath, inputFilenames, targetFilenameExtension, designatedPartNumber, windowSize, hopSize, featureIdentities, individualOutputFilenameSuffix, resultsOutputFilename, problemsOutputFilename):
    resultsOutputFile = open(str(resultsOutputFilename), 'w') # start writing the results file.
    resultsOutputFile.close() # stop writing the results file.
    problemsOutputFile = open(str(problemsOutputFilename), 'w') # start writing the record file.
    problemsOutputFile.write('path,filename'+'\n') # write the header row.
    currentFileNumber = 0  # for progress display.
    retrievedFileNumber = 0 # for retrieval results.
    for inputFilename in inputFilenames:
        currentFileNumber += 1 # for progress display.
        individualOutputFilename = inputFilename + ' - ' + individualOutputFilenameSuffix
        individualOutputFile = open(str(individualOutputFilename), 'w') # start writing the individual file.
        individualOutputFile.close() # stop writing the results file but will append later.
        startMeasure = 0 # to excerpt from the start measure to (not "before") the end measure #TODO: change to 1?
        endMeasure = windowSize - 1 # first end measure (included in the window)
        while True: # windowing
            try:
                parsingResult = music21.converter.parse(inputFilename)
            except:
                problemsOutputFile.write(childPath+','+inputFilename+'\n')
                print('failed to parse file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': '+inputFilename+' at '+str(datetime.datetime.now())) # display the filename.
                break
            else:
                partNumber, measureNumber = countMeasure(parsingResult)
                score = music21.stream.Score()
                if designatedPartNumber > 0: # extract single part
                    #TODO: Don't excerpt the measure 0 when there is no pickup measure. Maybe use Stream.recurse. cf. http://web.mit.edu/music21/doc/moduleReference/moduleStream.html
                    score.append(parsingResult[designatedPartNumber - 1].measures(startMeasure, endMeasure)[0:-1]) # the last element of list is redundant
                    # In music21, parts count from zero.
                    # Most pieces (that don’t have pickup measures) begin with measure 1, not zero.
                    # cf. http://web.mit.edu/music21/doc/usersGuide/usersGuide_06_stream2.html
                    # Stream.measures and measure.number cf. http://web.mit.edu/music21/doc/moduleReference/moduleStream.html#music21.stream.Stream.measures
                else:
                    for i in range(0, partNumber): # extract all parts
                        score.append(parsingResult[i].measures(startMeasure, endMeasure)[0:-1]) # the last element of list is redundant
                retrieve(individualOutputFilename, score, featureIdentities)
                startMeasure += hopSize # following start measure. Disable this line to activate the cumulative windowing.
                endMeasure += hopSize # following end measure.
                if endMeasure > measureNumber: # beyond the score.
                    retrievedFileNumber += 1 # for retrieval results.
                    correlate(childPath, inputFilename, individualOutputFilename, resultsOutputFilename, len(featureIdentities), retrievedFileNumber)
                    print('finished file '+str(currentFileNumber)+' of '+str(len(inputFilenames))+': '+inputFilename+' at '+str(datetime.datetime.now())) # progress display.
                    break
    problemsOutputFile.close() # stop writing the rocord file.

def countMeasure(parsingResult, partNumber=0, measureNumber=0): # count numbers of part(s) and measure(s)
    for thisObject in parsingResult:
        if isinstance(thisObject, music21.stream.Part): # count the amount of part(s).
            partNumber += 1
            if partNumber == 1: # only count measureNumber in the 1st part
                partNumber, measureNumber = countMeasure(thisObject, partNumber, measureNumber) # recursion
        elif isinstance(thisObject, music21.stream.Measure): # count the amount of measure(s)
            measureNumber += 1
    return partNumber, measureNumber

def retrieve(individualOutputFilename, score, featureIdentities): # retrieve features from the excerpt of sample
    dataSet = music21.features.DataSet(classLabel='test')
    featureSet = music21.features.extractorsById(featureIdentities) # retrieve selected features
    dataSet.addFeatureExtractors(featureSet) # add feature extractors
    try:
        dataSet.addData(score) # add a sample stream
    except:
        print('failed to add' +str(score)+' at ' + str(datetime.datetime.now()))
    # print('start to processe at '+str(datetime.datetime.now()))
    dataSet.process() # process the sample
    # print('finished processing at '+str(datetime.datetime.now()))
    tempList = dataSet.getString('csv')
    individualOutputFile = open(str(individualOutputFilename), 'a') # start appending the individual file.
    individualOutputFile.write(tempList) # intentionally omit new line here for easier text processing later.
    individualOutputFile.close() # stop appending the individual file.

def correlate(childPath, inputFilename, individualOutputFilename, resultsOutputFilename, featureIdentitiesListLength, retrievedFileNumber): # retrieve correlations from current sample and append to output file
    with open(individualOutputFilename) as target: # open(targetFilename, newline='')
        table = list(csv.reader(target)) # copy target to a nested list
    # step 1 of 4: values
    for i in range(len(table)):
        if i >= 1: # omit the first row
            for j in range(len(table[i])):
                if j >= 1 and j <= featureIdentitiesListLength: # omit the first column and the last columns
                    try:
                        table[i][j] = float(table[i][j]) # (row, cloumn) coordinate
                    except: # TODO: How to handle incorrect data format?
                        print(i,j) # display the coordinate of problematic value
    # step 2 of 4: transposition
    rowAmount = len(table)
    columnAmount = len(table[0])
    transposedTable = []
    for m in range(columnAmount):
        if m >= 1 and m <= featureIdentitiesListLength: # omit the first column and the last columns
            tempList = [] # temporary list for statistical operation
            for n in range(rowAmount):
                if n >= 1: # omit the first row
                    tempList.append(table[n][m]) # (row, cloumn) coordinate, so it is (n, m) instead of (m, n)
            transposedTable.append(tempList) # combine all temporary lists and consequently transpose the original table
    # step 3 of 4: summaries
    results = []
    for p in range(0, len(transposedTable) - 1):
        for q in range(p + 1, len(transposedTable)):
            correlation, pValue = scipy.stats.pearsonr(transposedTable[p], transposedTable[q])
            results.append(correlation) # correlation coefficient 
            results.append(pValue)# 2-tailed p-value
    results.insert(0, childPath) # insert dataset path into the list
    results.insert(1, inputFilename) # insert dataset sample amount into the list
    # step 4 of 4: output
    resultsOutputFile = open(str(resultsOutputFilename), 'a') # start appending the results file.
    if retrievedFileNumber == 1: # only create and append headers for once.
        resultsOutputFile.write(str(createHeaders(table))+'\n') # append the header row.
    resultsOutputFile.write(str(results)+'\n') # append results.
    resultsOutputFile.close() # stop appending the results file.

def createHeaders(table): # create headers for results output file
    tempHeaders = table[0][1:-1] # partially copy the first row
    finalHeaders = []
    for s in range(0, len(tempHeaders) - 1):
        for t in range(s + 1, len(tempHeaders)):
            finalHeaders.append('corr.: '+tempHeaders[s]+' and '+tempHeaders[t])
            finalHeaders.append('pValue: '+tempHeaders[s]+' and '+tempHeaders[t])
    finalHeaders.insert(0, 'path') # insert into the list
    finalHeaders.insert(1, 'filename') # insert into the list
    return finalHeaders

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()