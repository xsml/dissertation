"""
@author Hsin-Ming Lin. winter 2016.
@brief Extract susceptibility from a frequency value list.
"""
import math # to calculate log

class Melody:

    def __init__(self, inputFrequencyList, increment=0.01, startPoint=0, endPoint=0):
        if increment <= 0 or increment > 1: # too small or too large
            increment = 0.1
            print('too small or too large "increment", corrected to 0.1')
        if startPoint < 0: # too small
            startPoint = 0
            print('too small "start point", corrected to 0')
        if endPoint < 0: # too small
            endPoint = 0 # no segmentation
            print('too small "end point", corrected to 0')
        self._frequencies = inputFrequencyList[:] # copy the frequency value list
        self._length = len(self._frequencies) # extracted melody length before excerption; also = len(self._pitches)
        self._excerpt(startPoint, endPoint) # excerpt part of the original melody
        self._highestFrequency = max(self._frequencies)
        self._lowestFrequency = min(self._frequencies)
        self._registerWidth = math.log(self._highestFrequency/self._lowestFrequency, 2) # across how many octave(s)
        self._centralFrequency = 2**(math.log(self._lowestFrequency, 2)+(self._registerWidth/2))
        self._frequencies.insert(0, self._centralFrequency) # insert before the excerpt
        self._frequencyRatios = []
        self._calculateFrequencyRatios()
        self._energyRatios = []
        self._calculateEnergyRatios()
        self._initialMaxEnergyRatio = max(self._energyRatios)*4
        self._initialMinEnergyRatio = min(self._energyRatios)/4
        self._energyRatioIntervals = []
        self._calculateEnergyRatioIntervals()
        self._largestEnergyRatioInterval = max(max(self._energyRatioIntervals[1:]), abs(min(self._energyRatioIntervals[1:])))
        self._susceptibility = 0 # initialization
        self._calculateSusceptibility(increment)

    def _excerpt(self, startPoint, endPoint):
        if endPoint > 0: # need to segment
            frequencies = self._frequencies[startPoint:endPoint] # excerpt before the end point
            del self._frequencies[:]
            self._frequencies = frequencies[:]
            del frequencies[:]

    def _calculateFrequencyRatios(self):
        for frequency in self._frequencies:
            self._frequencyRatios.append(frequency/self._frequencies[0])

    def _calculateEnergyRatios(self):
        for frequencyRatio in self._frequencyRatios:
            self._energyRatios.append(frequencyRatio**2)

    def _calculateEnergyRatioIntervals(self):
        self._energyRatioIntervals.append(None) # offset for central frequency
        for i in range(1, len(self._frequencies)):
            self._energyRatioIntervals.append(self._energyRatios[i]-self._energyRatios[i-1])

    def _calculateMaxEnergyRatios(self):
        self.maxEnergyRatios.append(None) # offset for central frequency
        self.maxEnergyRatios.append(self._initialMaxEnergyRatio)
        judgement = True # presumedly passed maximal threshold
        for i in range(2, len(self._frequencies)):
            maxEnergyRatio = self.maxEnergyRatios[i-1]-(self._energyRatioIntervals[i-1]*self._susceptibility)
            if self._energyRatios[i] <= maxEnergyRatio:
                self.maxEnergyRatios.append(maxEnergyRatio)
            else: # due to the too large susceptibility
                judgement = False # not passed maximal threshold
                break
        return judgement

    def _calculateMinEnergyRatios(self):
        self.minEnergyRatios.append(None) # offset for central frequency
        self.minEnergyRatios.append(self._initialMinEnergyRatio)
        judgement = True # presumedly passed maximal threshold
        for i in range(2, len(self._frequencies)):
            minEnergyRatio = 1/((1/self.minEnergyRatios[i-1])+(self._energyRatioIntervals[i-1]*self._susceptibility))
            if self._energyRatios[i] >= minEnergyRatio:
                self.minEnergyRatios.append(minEnergyRatio)
            else: # due to the too large susceptibility
                judgement = False # not passed minimal threshold
                break
        return judgement

    def _calculateSusceptibility(self, increment):
        while True:
            self.maxEnergyRatios = []
            self.minEnergyRatios = []
            if self._calculateMaxEnergyRatios() is True and self._calculateMinEnergyRatios() is True:
                self._susceptibility += increment # iterate to approch the largest susceptibility
            else:
                self._susceptibility -= increment # one step backward
                if self._susceptibility < 0:
                    self._susceptibility = 0 # minimal value = 0
                break

    def getLength(self):  
        return self._length
    def getHighestFrequency(self):  
        return self._highestFrequency
    def getLowestFrequency(self):  
        return self._lowestFrequency
    def getRegisterWidth(self):  
        return self._registerWidth
    def getCentralFrequency(self):  
        return self._centralFrequency
    def getLargestEnergyRatioInterval(self):
        return self._largestEnergyRatioInterval
    def getSusceptibility(self):
        return self._susceptibility

if __name__ == '__main__':
    print('This is only a class. Nothing to do.')