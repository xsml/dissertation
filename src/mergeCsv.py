"""
@author Hsin-Ming Lin. winter 2016
@brief Merge all csv files respectively in target folders.
"""
import datetime
import os

def main(): # user settings.
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\Dataset\Analysis\Music21 Global Features\Vocal"
    targetFilenameExtension = ".csv"
    resultsOutputFilename = "Merged.csv"
    dig(motherPath, targetFilenameExtension, resultsOutputFilename)
    displayTimestamp(initializationTimestamp)

def dig(motherPath, targetFilenameExtension, resultsOutputFilename):
    childPaths = [] # to read files and write results    
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        os.chdir(childPaths[i])
        print('entered '+childPaths[i]) # progress display.
        inputFilenames = []
        for filename in os.listdir(childPaths[i]):
            if filename.endswith(targetFilenameExtension):
                inputFilenames.append(filename)
        if len(inputFilenames) > 0: # not an empty list.
            merge(resultsOutputFilename, inputFilenames)

def merge(resultsOutputFilename, inputFilenames):
    fileCounter = 0 # Read all lines in the first file; skip the first line in all the rest files.
    for inputFilename in inputFilenames:
        fileCounter += 1
        if fileCounter == 1: # the 1st file
            resultsOutputFile = open(str(resultsOutputFilename), 'w') # start writing the results file.
            for line in open(inputFilename):
                resultsOutputFile.write(line)
            resultsOutputFile.close() # stop writing the results file.
        else: # the rest files
            resultsOutputFile = open(str(resultsOutputFilename), 'a') # start writing the results file.
            lineCounter = 0
            for line in open(inputFilename):
                lineCounter += 1
                if lineCounter > 1: # not the header line
                    resultsOutputFile.write(line)
            resultsOutputFile.close() # stop writing the results file.

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()