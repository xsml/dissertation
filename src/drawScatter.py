"""
@author Hsin-Ming Lin. spring 2017.
@brief Read numerous csv files to draw a scatter figure.
"""
import datetime
import matplotlib.pyplot
import os
import pandas

def main(): # user settings
    initializationTimestamp = datetime.datetime.now()
    path = "D:\\Depository\\Curriculum\\UCSD\\Research\\Retrieval\\20161222 Custom Global Feature from 91 Datasets\\Comparison"
    targetFilenameExtension = ".csv"
    columnNameX = 'susceptibility'
    columnNameY = 'register width'
    aggregate(path, targetFilenameExtension, columnNameX, columnNameY)
    displayTimestamp(initializationTimestamp)

def aggregate(path, targetFilenameExtension, columnNameX, columnNameY): # aggregate filenames
    os.chdir(path)
    inputFilenames = []
    for filename in os.listdir(path):
        if filename.endswith(targetFilenameExtension):
            inputFilenames.append(filename)
    if len(inputFilenames) > 0: # not an empty list.
        read(inputFilenames, columnNameX, columnNameY)

def read(inputFilenames, columnNameX, columnNameY): # read csv files
    x = []
    y = []
    for inputFilename in inputFilenames:
        data = pandas.read_csv(inputFilename)
        x.append(data[columnNameX])
        y.append(data[columnNameY])
    draw(inputFilenames, x, y, columnNameX, columnNameY)

def draw(inputFilenames, x, y, columnNameX, columnNameY): # draw the figure
    labels = []
    colors = ['red', 'blue', 'black', 'green', 'magenta', 'cyan', 'yellow']
    markers =['+', '^', '.', 'v', '*', 'd', 'x']
    for i in range(len(inputFilenames)): # 0 <= i <= 6
        labels.append(inputFilenames[i][:-4]) # read the current filename as the lable
        matplotlib.pyplot.scatter(x[i], y[i], label=labels[i], color=colors[i], marker=markers[i])
    matplotlib.pyplot.xlabel(columnNameX)
    matplotlib.pyplot.ylabel(columnNameY)
    # matplotlib.pyplot.title('Title')
    matplotlib.pyplot.legend()
    matplotlib.pyplot.show()

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))

if __name__ == '__main__':
    main()