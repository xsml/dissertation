"""
@author Hsin-Ming Lin. spring 2016.
@brief Retrieve a music21-windowed feature from files in target folders.
disadvantage: window size unit = quarter note
http://web.mit.edu/music21/doc/moduleReference/moduleAnalysisWindowed.html
"""
import datetime
import music21
import os

class FeatureWrapper: # https://groups.google.com/d/msg/music21list/O-P_MC_-vI8/yRBLN1WL3CkJ
    def __init__(self, featureExtrator):
        self.featureExtrator = featureExtrator
        return
    def process(self, sStream):
        result = self.featureExtrator(sStream).extract().vector
        return result, None # data[i], color[i]; in music21\analysis\windowed.py

def main(): # user settings.
    initializationTimestamp = datetime.datetime.now()
    motherPath = "D:\Dataset\Test"
    targetFilenameExtension = ".mxl"
    targetFeature = music21.features.jSymbolic.RangeFeature
    dig(motherPath, targetFilenameExtension, targetFeature)
    displayTimestamp(initializationTimestamp)

def dig(motherPath, targetFilenameExtension, targetFeature):
    childPaths = [] # to read files and write results
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        retrieve(childPaths[i], targetFilenameExtension, targetFeature)

def retrieve(childPath, targetFilenameExtension, targetFeature):
    os.chdir(childPath)
    print('entered '+childPath) # progress display.
    dataSet = music21.features.DataSet(classLabel='test')
    featureSet = music21.features.extractorsById('all') # all features
    dataSet.addFeatureExtractors(featureSet) # add feature extractors
    for filename in os.listdir(childPath):
        if filename.endswith(targetFilenameExtension):
            try:
                parsingResult = music21.converter.parse(filename) # a problematic file may raise exception here
                print('parsed ' +filename+' at '+str(datetime.datetime.now()))
            except:
                print('failed to parse '+filename+' at '+str(datetime.datetime.now()))
            else:
                wa = music21.analysis.windowed.WindowedAnalysis(parsingResult, FeatureWrapper(targetFeature))
                print('start to processe '+filename+' at '+str(datetime.datetime.now()))
                print(wa.process(1, 2)) # default: (minWindow=1, maxWindow=1, windowStepSize=1, windowType='overlap', includeTotalWindow=True)
                print('finished processing '+filename+' at '+str(datetime.datetime.now()))

def displayTimestamp(initializationTimestamp):
    print('initiated at ' + str(initializationTimestamp))
    print('completed at ' + str(datetime.datetime.now()))
    print('duration = ' + str(datetime.datetime.now() - initializationTimestamp))
                
if __name__ == '__main__':
    main()