import numpy # to calculate tessitura
import scipy.stats # to calculate mobility

melody = [57, 54, 57, 57, 55, 54, 55, 55, 52, 52, 54, 52, 54, 55, 57, 57, 54, 57, 62, 61, 59, 57, 59, 56, 57, 57, 55, 54, 57, 55, 54, 62, 61, 59, 62, 61, 59, 57, 57, 57, 66, 64, 62, 64, 62, 61, 64, 62]
tessitura = numpy.std(melody)
antecedents = melody[:-1]
consequents = melody[1:]
mobility = scipy.stats.pearsonr(antecedents, consequents)[0]
print('tessitura ='), tessitura
print('mobility ='), mobility