"""
@author Hsin-Ming Lin. spring 2016.
@brief Parse a file and retrieve its local features.
"""
import datetime
import music21
import os

def main():
    motherPath = "D:\\Dataset\\Validation"
    inputFilename = "sonata08-3.krn"
    resultsOutputFilename = "Retrieval (music21).csv" # to write results into childPath.
    resultsOutputFilename = inputFilename + ' - ' + resultsOutputFilename
#     designatedPartNumber = 1 # 0 = combine all parts; 1 = 1st part, 2 = 2nd part, ano so on.
#     windowSize = 2
    os.chdir(motherPath)
    parsingResult = music21.converter.parse(inputFilename)
#    parsingResult.show('text')
#    parsingResult.show()
    partNumber, measureNumber = countMeasure(parsingResult)
    print(partNumber, measureNumber)
    score = music21.stream.Score()
    for i in range(0, partNumber):
        score.append(parsingResult[i].measures(0,1)[0:-1]) # the last element of list is redundant
        # "A a request for measures 4 through 10 will return 7 Measures, numbers 4 through 10."
        # 2017-06-04 test here: 4~10 retuened only 6 mearues, number 4 through 9, but parsingResult.mesures(,) is correct.
#    score.show('midi')
    score.show('text')
#    score.show()
    # parsingResult.show('text')
    # print(parsingResult[0][0][4].pitch.midi)
    # parsingResult.show() # or parsingResult.show('musicxml')
    resultsOutputFile = open(str(resultsOutputFilename), 'w') # start writing the results file.
    resultsOutputFile.close()
    # retrieve(resultsOutputFilename, score)

def countMeasure(parsingResult, partNumber=0, measureNumber=0):
    for thisObject in parsingResult: # count the amount of part(s).
        if isinstance(thisObject, music21.stream.Part):
            partNumber += 1
            if partNumber == 1: # only count the 1st part
                partNumber, measureNumber = countMeasure(thisObject, partNumber, measureNumber) # recursion
        elif isinstance(thisObject, music21.stream.Measure): # count the amount of measure(s)
            measureNumber += 1
    return partNumber, measureNumber

def retrieve(resultsOutputFilename, score):
    dataSet = music21.features.DataSet(classLabel='test')
    featureSet = music21.features.extractorsById(['M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M9', 'M15', 'M17', 'M18', 'M19'])
    dataSet.addFeatureExtractors(featureSet) # add feature extractors
    try:
        dataSet.addData(score) # add a sample stream
        print('added '+str(score)+' at '+str(datetime.datetime.now()))
    except:
        print('failed to add' +str(score)+' at ' + str(datetime.datetime.now()))
    print('start to processe at '+str(datetime.datetime.now()))
    dataSet.process() # process the sample files
    print('finished processing at '+str(datetime.datetime.now()))
    tempList = dataSet.getString('csv')
    resultsOutputFile = open(str(resultsOutputFilename), 'a') # start appending the results file.
    resultsOutputFile.write(tempList)
    resultsOutputFile.close() # stop appending the summary file.

if __name__ == '__main__':
    main()