'''
reference
https://web.mit.edu/music21/doc/moduleReference/moduleMidiTranslate.html?highlight=streamtomidifile#music21.midi.translate.streamToMidiFile
'''
import music21
import os

sourcePath = 'D:\Dataset\Test'
inputFileName = 'han0001.krn'
outputName1 = inputFileName + '.mid'
outputName2 = inputFileName + '.xml'
os.chdir(sourcePath)
score = music21.converter.parse(inputFileName)
score.write('midi', outputName1)
score.write('musicxml', outputName2)

outputPathName1 = 'D:\Dataset\Test\Output.mid'
outputPathName2 = 'D:\Dataset\Test\Output.xml'
coreCorpus = music21.corpus.CoreCorpus()
a = coreCorpus.getComposer('bach', 'krn')
for l in len(a):
    score = music21.converter.parse(a[0])
    score.write('midi', outputPathName1)
    score.write('musicxml', outputPathName2)

midiFile = music21.midi.translate.streamToMidiFile(score)
midiFile.open('han0001.mid', 'wb')
midiFile.write()
midiFile.close()