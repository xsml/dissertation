"""
@author Hsin-Ming Lin. summer 2015.
@brief Check if there is any file can not feed to music21 features extractors.
"""
import music21
import os

def main(): # user settings.
    motherPath = "D:\\Depository\\Career\\Academia Sinica\\Project\\String Quartets\\Dataset\\B"
    targetFilenameExtension = ".krn"
    iterate(motherPath, targetFilenameExtension)

def iterate(motherPath, targetFilenameExtension):
    childPaths = [] # to read files and write results    
    for paths, dirNames, fileNames in os.walk(motherPath):
        if len(dirNames) == 0: # terminal directory
            childPaths.append(paths) # append this path
    for i in range(len(childPaths)):
        retrieve(childPaths[i], targetFilenameExtension)

def retrieve(childPath, targetFilenameExtension):
    os.chdir(childPath)
    print('entered '+childPath) # progress display.
    for filename in os.listdir(childPath):
        if filename.endswith(targetFilenameExtension):
            dataSet = music21.features.DataSet(classLabel='year')
            featureSet = music21.features.extractorsById('all')
            dataSet.addFeatureExtractors(featureSet)
            print('start adding ' + filename)
            dataSet.addData(filename) # check if the file can feed to music21 features extractors.

if __name__ == '__main__':
    main()